package net.customware.confluence.plugin.linkvalidator;

import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import org.randombits.confluence.support.LinkAssistant;
import org.randombits.confluence.support.ServletAssistant;
import org.randombits.confluence.support.legacy.LegacyConfluenceMacro;

public class LegacyLinkValidatorMacro extends LegacyConfluenceMacro {

    private LinkValidatorMacro newMacro;

    private static final RenderMode RENDER_MODE = RenderMode.suppress(RenderMode.F_FIRST_PARA + RenderMode.F_LINKS);

    public LegacyLinkValidatorMacro(ServletAssistant servletAssistant, XhtmlContent xhtmlContent, WebResourceManager webResourceManager, LinkAssistant linkAssistant) {
        newMacro = new LinkValidatorMacro(servletAssistant, xhtmlContent, webResourceManager, linkAssistant);
    }

    public RenderMode getBodyRenderMode() {
        return RENDER_MODE;
    }

    @Override
    protected Macro getXHtmlMacro() {
        return newMacro;
    }

    @Override
    public boolean hasBody() {
        return true;
    }
}
