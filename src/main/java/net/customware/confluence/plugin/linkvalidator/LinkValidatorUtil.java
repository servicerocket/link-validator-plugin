package net.customware.confluence.plugin.linkvalidator;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.HeadMethod;
import org.apache.log4j.Logger;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.spring.container.ContainerManager;

public class LinkValidatorUtil
{
    private static final Logger LOG                         = Logger.getLogger(LinkValidatorUtil.class);

    private static final String HTTPS_PROTOCOL              = "https";

    private static final String HTTP_PROTOCOL               = "http";

    private static final String INFO_ICON                   = "emoticons/information.gif";

    private static final String GOOD_ICON                   = "emoticons/check.gif";

    private static final String ERROR_ICON                  = "emoticons/warning.gif";

    private static final String REDIRECT_ICON               = "ref_16.gif";

    private static final String UNKNOWN_ICON                = "emoticons/help_16.gif";

    private static final String FORBIDDEN_ICON              = "emoticons/forbidden.gif";

    private static final String LINK_VALIDATOR_HEADER_VALUE = "1";

    private static final String LINK_VALIDATOR_HEADER       = "x-randombits-link-validator";

    static BootstrapManager     bootstrapManager;

    private LinkValidatorUtil()
    {
    }

    public static boolean isBeingValidated(HttpServletRequest request)
    {
        return LINK_VALIDATOR_HEADER_VALUE.equals(request.getHeader(LINK_VALIDATOR_HEADER));
    }

    public static String validate(URL url, boolean verbose, int timeout)
    {
        try
        {
            // Check that we're only validating HTTP(S) locations.
            if (!HTTP_PROTOCOL.equals(url.getProtocol()) && !HTTPS_PROTOCOL.equals(url.getProtocol()))
            {
                return createErrorIcon("Unsupported protocol: " + url.getProtocol(), verbose);
            }

            LOG.debug("connecting to url: " + url);

            HttpClient client = new HttpClient();
            client.setTimeout(timeout * 1000);
            HeadMethod method = new HeadMethod(url.toString());
            method.addRequestHeader(new Header(LINK_VALIDATOR_HEADER, LINK_VALIDATOR_HEADER_VALUE));

            client.executeMethod(method);

            // Read the body, even though we don't need it.
            method.getResponseBody();
            method.releaseConnection();

            return createHttpStatusIcon(method.getStatusCode(), method.getStatusText(), verbose);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return createErrorIcon("Invalid URL: " + e, verbose);
        }
        catch (UnknownHostException e)
        {
            return createErrorIcon("Unknown host: " + e.getLocalizedMessage(), verbose);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return createErrorIcon(e.getLocalizedMessage(), verbose);
        }
    }

    static String createHttpStatusIcon(int responseCode, String responseMessage, boolean verbose)
    {
        String icon = UNKNOWN_ICON;
        if (responseCode >= 100 && responseCode < 200)
            icon = INFO_ICON;
        else if (responseCode < 300)
            icon = GOOD_ICON;
        else if (responseCode < 400)
            icon = REDIRECT_ICON;
        else if (responseCode < 500)
            icon = FORBIDDEN_ICON;
        else if (responseCode < 600) icon = ERROR_ICON;

        return createIcon(icon, responseMessage + " (" + responseCode + ")", verbose);
    }

    private static String createIcon(String iconPath, String status, boolean verbose)
    {
        StringBuffer out = new StringBuffer();
        out.append("<img");
        out.append(" src='").append(getContextPath()).append("/images/icons/").append(iconPath).append("'");
        if (status != null) out.append(" title='").append(GeneralUtil.escapeXml(status)).append("'");
        out.append("/>");

        if (verbose && status != null) out.append(" ").append(GeneralUtil.escapeXml(status));

        return out.toString();
    }

    private static String getContextPath()
    {
        return getBootstrapManager().getWebAppContextPath();
    }

    public static BootstrapManager getBootstrapManager()
    {
        if (bootstrapManager == null)
            bootstrapManager = (BootstrapManager) ContainerManager.getComponent("bootstrapManager");
        return bootstrapManager;
    }

    public static String createInfoIcon(String status, boolean verbose)
    {
        return createIcon(INFO_ICON, status, verbose);
    }

    public static String createGoodIcon(String status, boolean verbose)
    {
        return createIcon(GOOD_ICON, status, verbose);
    }

    public static String createUnknownIcon(String status, boolean verbose)
    {
        return createIcon(UNKNOWN_ICON, status, verbose);
    }

    public static String createErrorIcon(String status, boolean verbose)
    {
        return createIcon(ERROR_ICON, status, verbose);
    }

    public static String createRedirectIcon(String status, boolean verbose)
    {
        return createIcon(REDIRECT_ICON, status, verbose);
    }

    public static String createForbiddenIcon(String status, boolean verbose)
    {
        return createIcon(FORBIDDEN_ICON, status, verbose);
    }

}
