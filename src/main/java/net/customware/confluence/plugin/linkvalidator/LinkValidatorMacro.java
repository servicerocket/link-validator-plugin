package net.customware.confluence.plugin.linkvalidator;

import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.webresource.WebResourceManager;
import org.apache.commons.lang.StringUtils;
import org.randombits.confluence.support.ConfluenceMacro;
import org.randombits.confluence.support.LinkAssistant;
import org.randombits.confluence.support.MacroInfo;
import org.randombits.confluence.support.ServletAssistant;
import org.randombits.storage.IndexedStorage;

public class LinkValidatorMacro extends ConfluenceMacro {

    private final WebResourceManager webResourceManager;

    private final LinkAssistant linkAssistant;

    public LinkValidatorMacro(ServletAssistant servletAssistant, XhtmlContent xhtmlContent, WebResourceManager webResourceManager, LinkAssistant linkAssistant) {
        super(servletAssistant, xhtmlContent);
        this.webResourceManager = webResourceManager;
        this.linkAssistant = linkAssistant;
    }

    protected String execute(MacroInfo info) throws MacroExecutionException {
        IndexedStorage params = info.getMacroParams();
        String link = params.getString("url", null);
        if (StringUtils.isEmpty(link)) {
            throw new MacroExecutionException("Please supply the URL to validate.");
        }

        // If true, output the message in full.
        boolean verbose = params.getBoolean("verbose", false);
        // The timeout in seconds.
        int timeout = params.getInteger("timeout", 5);

        StringBuffer out = new StringBuffer();

        webResourceManager.requireResource("net.customware.confluence.plugin.linkvalidator:linkjs");
        openScript(out);
        out.append("LinkValidator.contextPath = \"").append(linkAssistant.getContextPath()).append("\";\n");
        closeScript(out);

        out.append("<div class='validatorElement'");
        out.append(" url='").append(GeneralUtil.escapeXml(link)).append("'");
        out.append(" timeout='").append(timeout).append("'");
        out.append(" verbose='").append(verbose).append("'");
        out.append(">");

        out.append(LinkValidatorUtil.createUnknownIcon("Unvalidated", verbose));

        out.append("</div>");
        return out.toString();
    }

    private void closeScript(StringBuffer out) {
        out.append("//]]>\n");
        out.append("</script>");
    }

    private void openScript(StringBuffer out) {
        out.append("<script type='text/javascript'");
        out.append(">");
        out.append("//<![CDATA[\n");
    }

    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
